#!/usr/bin/perl

# This script handles the actual text-processing to convert UNT's
# records in the schedule of classes to CSV records.

# You know... it would be simpler if the registrar could just share
# the data in tabular form.

# Also, I know this isn't a paradigm of great programming. I use the
# state of the input buffer implicitly a lot among other things.
# Heck, does anyone even write anything in Perl anymore?

# Anyways, if you're interested, feel free to make changes or adapt
# this program however you please.

use warnings;
no warnings 'uninitialized';
use strict;

# Initialize any variables that may carry over across input files
# Just using a plain vanilla array for ordering


my @course;		# Dept, number, title
my @section;	# Number, Code, Type, Prof
my @locate; 	# Days, Start time, End time, Room
my $nextgen;	# For storing internet instruction info
my $restricted; # Go ahead and record if the course is restricted

# First pull the base filename parameter from the argument list
my $outfile = shift(@ARGV);
# Open the default output sheet (may wind up blank)
open SHEET, ">>", "../$outfile.csv" or die $!;

# Now with only globbed files for arguments, loop over the inputs
foreach (@ARGV) {

    # Open the current input file
    open SOURCE, $_ or die $!;
    # And start scanning it...
    while (<SOURCE>) {

        # Load the current line into a variable
        my $line = $_;

        # From here on out, we extract info from different lines
        # If we come to a new record, we flush the buffer correctly

        # If you find session info, grab the label...
        if ($line =~ m;(\S+.*\S)\sSession(\s\S+)?;) {

            # First store the capture groups from the match
            my $session = "$1$2";
            # Flush everything before switching files
            flush("all");

            # and switch to a fresh sheet for that session
            close SHEET;
            open SHEET, ">>", "../$outfile $session.csv" or die $!;
        }

        # but if you match a course name, pull out those fields
        elsif ($line =~ m;^\s*
                          ([A-Z]{3,4})\s
                          (\d{4})\s*
                          ([A-Z].*[A-Z])
                          \s*$;x) {

			# Use a temporary array for handling return values
			my @tmparray = ($1, $2, "\"$3\"");

            # Flush the stored record from the buffer completely
            flush("all");
            # Then push to @course
            @course = @tmparray;
        }

        # ... or match on a specific section & extract the info
        # This is the involved one
        elsif ($line =~ m;\s*(\d{3})
                       \s+\((\d{5})\)
                       \s+(CRE|LAB|REC)
                       # Ignoring V tosses out irregular courses
                       \s\d\.\d;x) {

			my @tmparray = ($1, $2, $3);
            # 1st flush just preceding section info from the buffer
            flush("section");
			# Store the first few values ...
            @section = @tmparray;

            # Then chop off all of the scanned part of the line
            $line =~ s;.*(CRE|LAB|REC)\s\d\.\d\s*(\S.*)?;$2;;

            # If a regular class, grab time & place
            if ($line =~ m;([MTWRF]+)\s+
                           (\d\d:\d\d\s(?:am|pm))-
                           (\d\d:\d\d\s(?:am|pm))\s+
                           (\w+(?:\s\w+)?);x) {

				push (@locate, ($1, "\"$2\"", "\"$3\"", "\"$4\""));
            }
            else { push (@locate, ",,,"); }

            # Grab prof info from the end of the line
            if ($line =~ m;([A-Z][a-z]\S*
						   (?:\sJr)?
						   (?:\s[A-Z])?)\s*$;x) {
				push (@section, "\"$1\"");
			}
			# Double quotes make sure prof field isn't skipped
            else { push (@section, "\"\""); }
        }

        # Hunt down online course category here
        elsif ($line =~ m;(BLENDED|WEB(?:\s|-)BASED|HYBRID);) {

            # No need to flush the buffer here
            $nextgen = "\"$1\"";
        }
        
        # See if the course is restricted
        elsif ($line =~ m;(RESTRICTED);) {
			$restricted = "\"$1\"";
		}
    }

    # Flush the buffer entirely before exiting current input file
    flush("all");
    # Close the schedule file to be safe, then repeat
    close SOURCE;
}

# Here's where we handle actually printing & resetting the records
sub flush {

	# First shortcircuit the routine if the primary key's empty
	my $sectsize = @section;
	if (not $sectsize) { $nextgen = ""; $restricted = ""; }

	else {

		# I know it's not good design, but globals are simple
		my $coursestr = join(",", @course);
		my $sectionstr = join(",", @section);
		my $locatestr = join(",", @locate);

		# Print the merged string to the output file
		print SHEET join(",", $coursestr, $sectionstr, $locatestr,
							  $nextgen, $restricted);
		print SHEET "\n";

		# Use the passed flag variable
		my $keep = shift;
		
		# Finally reinitialize the record correctly
		if ($keep eq "section") {
			undef @section; undef @locate;
			$nextgen = ""; $restricted = "";
		}
		else {
			undef @course; undef @section; undef @locate;
			$nextgen = ""; $restricted = "";
		}
	}
}
